import { LitElement, html } from "https://unpkg.com/lit-element?module";

customElements.define("eaas-localstorage", class extends LitElement {
    static get properties() {
        return {
            key: String,
        }
    }
    constructor() {
        super();
        addEventListener("storage", () => this.requestUpdate());
    }
    render() {
        return html`
        <label><slot></slot>
        <input type="text" @change="${this._onchange}" .value="${localStorage[this.key] || ""}" />
        </label>`;
    }
    _onchange({ target: { value } }) {
        if (!value) delete localStorage[this.key];
        else localStorage[this.key] = value;
    }
});
