import { LitElement, html } from "https://unpkg.com/lit-element?module";
import { until } from "https://unpkg.com/lit-html/directives/until.js?module";

export const fetchX = async (...args) => {
    let req = new Request(...args);
    req = new Request(req, { referrerPolicy: "no-referrer" });
    return fetch(`https://cors-anywhere.herokuapp.com/${req.url}`, req);
};

export const fetchDocument = async (...args) => {
    const innerHTML = await (await fetchX(...args)).text();
    return Object.assign(document.createElement("template"), { innerHTML }).content;
};

const getJobs = async project => {
    const getURL = a => String(new URL(a.getAttribute("href"), `${project}/`));

    const doc = await fetchDocument(`${project}/-/jobs`);
    const jobs = [...(doc).querySelectorAll("tr.build.commit")];
    const data = jobs.map(el => {
        const status = el.querySelector("a").title;
        const as = [...el.querySelectorAll(".branch-commit a")].map(getURL);
        const stage = el.querySelector("td:nth-of-type(4)").innerText.trim();
        const name = el.querySelector("td:nth-of-type(5)").innerText.trim();
        return {
            status, job: as[0], branch: as[1], commit: as[2], stage, name,
        };
    });
    return data;
}

const getPagesURL = projectURL => {
    const url = new URL(projectURL);
    const parts = url.pathname.split(/\/+/);
    url.host = `${parts[1]}.gitlab.io`;
    parts[1] = "-";
    url.pathname = parts.join("/");
    return String(url);
}

customElements.define("eaas-builds-link", class extends LitElement {
    static get properties() {
        return {
            href: String,
        }
    }
    render() {
        return html`
        <style>a { text-decoration: none; }</style>
        <a target="_blank" rel="noopener" href="${this.href}"><slot></slot></a>`;
    }
});

const okay = html`<span style="color: green">✓</span>`;
const fail = html`<span style="color: red">✗</span>`;

customElements.define("eaas-builds", class extends LitElement {
    static get properties() {
        return {
            project: String,
            path: String,
            stage: String,
            name: String,
        }
    }
    render() {
        return html`
        <style>
        h1 {
            font-size: 14pt;
        }
        </style>
        <h1>
        <slot></slot>:
        ${this.path} from
        <eaas-builds-link href="${this.project}">${
            this.project.split("/").slice(-2).join("/")}</eaas-builds-link>
        ${(this.stage || this.name) && `(${this.stage || ""}/${this.name || ""})`}
        </h1>
        ${until(this.render2(), "Loading ...")}`;
    }
    async render2() {
        let jobs = await getJobs(this.project);
        console.log(jobs);
        jobs = jobs.filter(({ stage, name }) =>
            (!this.stage || this.stage == stage) && (!this.name || this.name === name));
        console.log(jobs);
        return html`
        <table>
        ${jobs.map(({ status, job, branch, commit }) => html`
            <tr>
            <td style="line-height: 0" title="${status}"><eaas-builds-link href="${job}">${
                status.includes("Success") ? okay : fail}</eaas-builds-link></td>
            <td>
            <eaas-builds-link href="${getPagesURL(job)}/artifacts/${this.path}">${
                branch.split("/").pop()}</eaas-builds-link>
            <eaas-builds-link href="${commit}">(${commit.split("/").pop()})</eaas-builds-link></td>
            </tr>
        `)}
        </table>
        `;
    }
});
